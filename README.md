# Improving SORT

This is the "improve SORT" (weekly) exercise for FL8 week 4 day 4 by Gilad Rosenblatt.

### Run

Run `main.py` from `code` as working directory. Note that for the code to run, image files for all train sequences of the 2015 MOT Challenge must be available locally using the designated folder hierarchy (described below).

## Instructions

Several key parameters can be set in `__main__` to affect code execution:

**Execution flags**. Most notable is `use_similarity`, which activates a cascaded similarity-based metric to improve the assignment stage in SORT. It uses a normalized 0-1 score based on number of matching keypoints between crops (detected using SIFT). To do away with the IOU metric in the assignment stage altogether, activate the  `similarity_only` flag. the `write_video` flag saves results as a video file (can be activated from the command line using a `--video` argument when running `main.py`).

**SimSORT hyperparameters**. There are 3 hyperparameters to Similarity SORT on top of the standard SORT hyperparameters. The `normalize_score` toggles between using a normalized 0-1 value or the integer match count as a similarity score. `min_score` is threshold similarity score needed to pass an assignment between a track and a detection in the cascaded round. `parity_count` determines the "steepest rise" point for the normalization function `(match_count/parity_count) / 1 + (match_count/parity_count)` between match counts to normalized scores (a "soft threshold").

**SORT hyperparameters**. The `max_age` and `min_hits` hyperparameters of SORT, as describe in the [SORT paper](https://arxiv.org/pdf/1602.00763.pdf).

### Data

Data is not included in this repo. You should download the official [2015 MOT Challenge dataset](https://motchallenge.net/data/MOT15/), and unpack it in a `data/mot_benchmark` folder (or have a symbolic link for it there), keeping the internal folder hierarchy intact.

By default, the code is set to use alternative detections saved in `data/mot_detections` that comply with the MOT Challenge dataset folder hierarchy. This is indicated by the `use_alternative_detections` optional input set to default value `True` in the `get_detections` method of the `DataParser` class in the `parses.py` file in the `code` folder. To run on the official detections located in `data/mot_challenge` folder, set it to `False`.

There is an option to run directly from raw video files instead of image file sequences for each dataset, set setting the `from_video` flag to `True` in `main.py`'s `__main__`. To use it, videos need to be downloaded from the [2015 MOT Challenge website](https://motchallenge.net/data/MOT15/) and stored in a `data/mot_videos` folder, keeping with relative paths complying with the `phase`/`sequence`/`video_filename.webm` format, where `phase` is either train or test and `sequence` is the official name of the sequence (e.g., PETS09-S2L1).

A `data/output` folder is created automatically when `main.py` is executed to save the benchmark text files and/or output videos.

## Implementation

Similarity SORT is implemented by the `SimilaritySort` class in the `mots.py` file, located in the `code` folder. Implementation details can be found in its docstring, and those of its associated methods. The similarity score is calculated using SIFT in the `similarity` method, and is normalized to values in 0-1 by the `normalize_match_count` static method. Metric cascading in the assignment stage happens in the `_solve_assignment_problem` method, which is called from the `update` method of its superclass `Sort`. Extraction of image crops as slices of the current (saved) frame is done by `crop_current_frame`. Finally, `run` manages the execution of Similarity SORT by calling the superclass `run` method and prints stats on number of saved track drops.

## License

[WTFPL](http://www.wtfpl.net/)
