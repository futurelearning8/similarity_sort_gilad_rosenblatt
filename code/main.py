import os.path
import argparse
from parsers import DataParser, OutputParser
from handlers import ClassicSimilaritySortHandler, Configuration, SortHandler, DeepSimilaritySortHandler
from mots import ClassicSimilaritySort
from mots_deep import DeepSimilaritySort


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Process execution options")
    parser.add_argument(
        "--display",
        dest="display",
        help="Display multiple object tracker output to screen [False]",
        action="store_true"
    )
    parser.add_argument(
        "--video",
        dest="video",
        help="Write multiple object tracker output to video file [False]",
        action="store_true"
    )
    return parser.parse_args()


if __name__ == "__main__":

    # Run deep similarity sort?
    go_deep = True

    # Get execution flags from command line options.
    input_arguments = parse_arguments()

    # Set all execution flags (toggle "stub_feed" on for fastest runtime on SORT when not using SimilaritySORT).
    configs = Configuration(
        display=input_arguments.display,
        write_video=input_arguments.video,
        write_benchmark=True,
        stub_feed=False,
        from_video=False
    )

    # Set SORT hyperparameters.
    sort_kwargs = dict(
        max_age=5,  # Higher "max_age" helps reclaim trackers after occlusions (increases association recall).
        min_hits=3
    )

    # Set similarity SORT hyperparameters.
    parity_count = 3
    min_score = ClassicSimilaritySort.normalize_match_count(
        number_of_matches=8,  # 8+ good keypoint matches between crops to keep tracker-detection assignment (2nd round).
        match_count_to_parity=parity_count  # Normalized score "rises most steeply" at this number of good matches.
    )
    similarity_sort_kwargs = dict(
        similarity_first=False,
        cascade=True,  # If both cascade and similarity_first are False it runs SORT as usual (no similarity metric).
        min_score=min_score
    )

    # Set classic similarity SORT hyperparameters.
    classic_similarity_sort_kwargs = dict(
        normalize_score=True,
        parity_count=parity_count
    )

    # Assert that image data folder is available (print a download link to the dataset if not found).
    if not configs.stub_feed:
        assert os.path.exists(DataParser.get_base_path()), \
            f"Missing link to MOT challenge data!\n" \
            f"Please create a symbolic link to the MOT challenge data\n" \
            f"(https://motchallenge.net/data/MOT15/) using:\n" \
            f"$ ln -s /path/to/mot_2015_challenge/data {DataParser.get_base_path()}"

    # Create a folder to store output data files if none exists.
    if configs.write_benchmark and not os.path.exists(OutputParser.get_data_base_path()):
        os.makedirs(OutputParser.get_data_base_path())

    # Create a folder to store output video files if none exists.
    if configs.write_video and not os.path.exists(OutputParser.get_video_base_path()):
        os.makedirs(OutputParser.get_video_base_path())

    # Run multiple object tracking.
    if go_deep:
        similarity_sort_kwargs["min_score"] = 0.33
        similarity_sort_kwargs["similarity_first"] = True
        similarity_sort_kwargs["cascade"] = True
        handler = DeepSimilaritySortHandler(configuration=configs, **sort_kwargs, **similarity_sort_kwargs)
    else:
        handler = ClassicSimilaritySortHandler(
            configuration=configs,
            **sort_kwargs,
            **similarity_sort_kwargs,
            **classic_similarity_sort_kwargs
        )
    # handler = SortHandler(configuration=configs, **sort_kwargs)  # Just run standard SORT.
    handler.sequences = DataParser.train_sequences[0:1]  # Limit execution to first train sequence ("PETS09-S2L1").
    handler.execute()
