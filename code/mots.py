import inspect
import numpy as np
import cv2
from abc import ABC, abstractmethod
from scipy.optimize import linear_sum_assignment
from utils import TimerUtility, SyncIteratorWithCallbacks as SyncIteratorWithCallbacksMixin
from boxops import IOU, BoxMetric
from trackers import KalmanBoxTracker
from feeds import FeedIteratorWithCallbacks
from plotters import DisplayPlotter, VideoPlotter
from detectors import Detector
from parsers import OutputParser


class MultiObjectTracker(SyncIteratorWithCallbacksMixin, ABC):
    """
    A multiple object tracker that listens to a feed iterator w/callbacks and plots frames with bounding boxes around
    tracked objects while the feed is active and performs cleanup once feed terminates. Implementing this class only
    requires to implement the _update method that updates the tracker bounding boxes given detector generated
    bounding boxes from current image (and a method to access those tracker bounding boxes).
    """

    def __init__(self, feed, detector, display=False, write_video=False, write_benchmark=True):
        """
        Register self to feed with the next frame callback function being the update method.

        :param FeedIteratorWithCallbacks feed: image feed iterator that allows to register listener callbacks.
        :param Detector detector: a detector that detects object in an image (has a detect method that returns boxes).
        :param bool display: if True displays the images and trackers in each frame.
        :param bool write_video: if True writes the tracking results to a video file.
        :param bool write_benchmark: if True writes benchmark output to a text file.
        """

        # Save feed iterator.
        self._feed = feed

        # Save detector object.
        self._detector = detector

        # Save display feed and write video/benchmark flags.
        self._display = display
        self._write_video = write_video
        self._write_benchmark = write_benchmark

        # Add plotters according to display and write_video flags.
        self._plotters = []
        if self._display:
            self._plotters.append(DisplayPlotter())
        if self._write_video:
            self._plotters.append(VideoPlotter())

        # Create placeholder for current frame (updates at _process_next each iteration over the feed).
        self._current_frame = None

        # Sync iterator with callbacks: register self as listener to get notified on get_frame events using _process_x.
        self.sync(iter_wc=feed, keys=["frame"])

        # Cascade tracker-save count
        self._tracker_saves = 0

    def _process_start(self):
        for plotter in self._plotters:
            plotter.process_start(self._feed.name)

    def _process_next(self, frame):

        # Do nothing if frame was incorrectly retrieved (e.g., reached end of video file). Avoid ambiguous truth values.
        if not isinstance(frame, np.ndarray) and frame is None:
            return

        # Save current frame.
        self._current_frame = frame

        # Detect objects in current frame.
        detection_boxes = self._detector.detect(frame)

        # Update trackers using their current state and this frame's detection bounding boxes.
        self._update(detection_boxes)

        # Get updated tracker bounding boxes.
        tracker_boxes = self.tracker_boxes

        # Draw new frame with id-ed tracker bounding boxes. Kill feed if execution was interrupted.
        for plotter in self._plotters:
            if not plotter.process_next(image=frame, boxes=tracker_boxes):
                self._feed.terminate()  # For DisplayPlotter: user pressed the quit ('q') key.

    def _process_stop(self):
        for plotter in self._plotters:
            plotter.process_stop()

    def _write_to_file(self, output_file):
        """
        Write all tracker boxes to input file (using MOT benchmark format).
        For evaluation code run on top of written files see <https://github.com/dendorferpatrick/MOTChallengeEvalKit>
        For info on benchmarks for MOT see this paper: <https://link.springer.com/article/10.1007/s11263-020-01375-2>.

        :param output_file: file handler to output file.
        """
        for box in self.tracker_boxes:
            x1, y1, x2, y2, tracker_id = box
            w = x2 - x1  # Box width.
            h = y2 - y1  # Box height.
            frame_number = self._feed.index + 1
            # FIXME tracker_id is float instead of int (probably numpy auto-conversion).
            print(f"{frame_number},{int(tracker_id)},{x1:.2f},{y1:.2f},{w:.2f},{h:.2f},1,-1,-1,-1", file=output_file)

    def run(self, filename=None):
        """
        Run the multiple object tracker instance on the input feed by iteration the feed iterator w/callbacks,
        which implicitly updates trackers using listener mechanics (via _update). Optionally write tracker info to file.

        :param str filename: file path to which tracker boxes info will be written in each iteration.
        """
        print(f"Processing {self._feed.name}.")
        if filename and self._write_benchmark:
            # Process feed inside a context manager.
            with open(filename, 'w') as output_file:
                for _ in self._feed:  # Calls _update at each iteration.
                    self._write_to_file(output_file=output_file)
        else:
            for _ in self._feed:  # Calls _update at each iteration.
                continue

    @property
    @abstractmethod
    def tracker_boxes(self):
        """Return all tracker bounding boxes for current frame."""
        pass

    @abstractmethod
    def _update(self, detection_boxes):
        """
        Update the state of the multi-object tracker by one frame (called from _process_next_frame).

        :param np.ndarray detection_boxes: all bounding boxes returned by the detector for current frame.
        """
        pass


class Sort(MultiObjectTracker):
    """
    Implements a Simple Online Realtime Tracking (SORT) multiple object tracker (code written by Gilad Rosenblatt).
    The SORT algorithm was invented by Alex Bewley and published in this paper: <https://arxiv.org/pdf/1602.00763.pdf>.
    Its official implementation is found here: <https://github.com/abewley/sort/blob/master/sort.py> (GNU v3 license).
    It was benchmarked against the MOT 2015 challenge dataset found here: <https://motchallenge.net/data/MOT15/>.
    """

    def __init__(self, max_age=1, min_hits=3, metric=IOU, *args, **kwargs):
        """
        Instantiate a SORT object tracker.

        :param StubFeed feed: iterator on detection boxes for each frame in sequence that allows listener callback.
        :param int max_age: maximum allowed age for valid tracker.
        :param int min_hits: minimum number of hits needed to consider a tracker as valid.
        :param BoxMetric metric: a class that implements the BoxMetric interface (default: intersection over union).
        """

        # Register self to feed with the next frame callback function being the self _update method.
        super(Sort, self).__init__(*args, **kwargs)

        # Save SORT object tracker settings.
        self._max_age = max_age
        self._min_hits = min_hits

        # Initialize trackers list.
        self._trackers = []

        # Initialize frame counter.
        self._frame_count = 0

        # Save metric callable (go through setter method).
        self.metric = metric

        # Initialize current frame tracker boxes to an empty numpy array (of appropriate format).
        self._tracker_boxes = np.empty((0, 5))

    @property
    def tracker_boxes(self):
        """
        :return numpy.ndarray: array of current matched tracker bounding boxes in the format:

             [[x1, y1, x2, y2, ID],  --> tracker box 1, tracker ID 1
              [x1, y1, x2, y2, ID],  --> tracker box 2, tracker ID 2
              ...                 ,
              [x1, y1, x2, y2, ID]]  --> tracker box n, tracker ID n
        """
        return self._tracker_boxes

    @property
    def max_age(self):
        return self._max_age

    @property
    def min_hits(self):
        return self._min_hits

    @property
    def metric(self):
        return self._metric

    @metric.setter
    def metric(self, metric):
        """
        Assert type for property metric.

        :param BoxMetric metric: a class that implements the BoxMetric interface (with static distance method).
        """
        assert inspect.isclass(metric) and issubclass(metric, BoxMetric)
        self._metric = metric

    @TimerUtility.cumulative_timeit  # Counts cumulative runtime spent in this method.
    def _update(self, detection_boxes):
        """
        Update the state of the SORT multi-object tracker by one frame.

        :param numpy.ndarray detection_boxes: array of detection bounding boxes in the format:

             [[x1, y1, x2, y2, score],  --> detection box 1, detection score 1
              [x1, y1, x2, y2, score],  --> detection box 2, detection score 2
              ...                    ,
              [x1, y1, x2, y2, score]]  --> detection box n, detection score n

        :return numpy.ndarray: array of matched (valid) tracker bounding boxes in the format:

             [[x1, y1, x2, y2, ID],  --> tracker box 1, tracker ID 1
              [x1, y1, x2, y2, ID],  --> tracker box 2, tracker ID 2
              ...                 ,
              [x1, y1, x2, y2, ID]]  --> tracker box m, tracker ID m

        NOTE: This method must be called once for each frame even if there are no detections.
        NOTE: The number of trackers returned may differ from the number of detections provided.
        """

        # Update frame counter for this frame.
        self._frame_count += 1

        # Instantiate tracker boxes array and auxiliary parameters.
        tracker_boxes = np.zeros((len(self._trackers), 5))
        invalid_tracker_indices = []
        valid_trackers = []

        # Predict tracker boxes for this frame using previous frame tracker box positions.
        for t_index, tracker_box in enumerate(tracker_boxes):

            # Predict new tracker box position and use slice assignment to alter tracker_boxes.
            box = self._trackers[t_index].predict()[0]
            tracker_box[:] = [box[0], box[1], box[2], box[3], 0]

            # For invalid predictions mark tracker index for deletion.
            if np.isnan(box).any():
                invalid_tracker_indices.append(t_index)

        # Discard trackers with NaN predictions (state change).
        tracker_boxes = np.ma.compress_rows(np.ma.masked_invalid(tracker_boxes))
        for t_index in reversed(invalid_tracker_indices):
            self._trackers.pop(t_index)

        # Solve the assignment problem: match detection boxes to predicted tracker boxes.
        matches, unmatched_detections, unmatched_trackers = self._solve_assignment_problem(
            detection_boxes=detection_boxes,
            tracker_boxes=tracker_boxes,
        )

        # Update matched trackers with assigned detections.
        for t_index, tracker in enumerate(self._trackers):
            if t_index not in unmatched_trackers:
                d = matches[np.where(matches[:, 1] == t_index)[0], 0]
                tracker.update(detection_boxes[d, :][0])

        # Create and initialize new trackers for unmatched detections.
        for d_index in unmatched_detections:
            tracker = KalmanBoxTracker(detection_boxes[d_index, :])
            self._trackers.append(tracker)

        # Iterate over all potentially valid trackers in reversed order.
        t_index = len(self._trackers)
        for tracker in reversed(self._trackers):
            t_index -= 1

            # Append valid trackers and their ID to output list in [box, ID + 1] row format.
            if tracker.time_since_update == 0 and (tracker.hit_streak >= self._min_hits
                                                   or self._frame_count <= self._min_hits):
                valid_trackers.append(np.concatenate((
                    tracker.box[0],
                    [tracker.id + 1]  # +1 as MOT benchmark requires positive values.
                )).reshape(1, -1))

            # Discard old trackers that have not been updated for a while.
            if tracker.time_since_update > self.max_age:
                self._trackers.pop(t_index)

        # Update current frame tracker boxes to valid tracker boxes and IDs.
        if not valid_trackers:
            self._tracker_boxes = np.empty((0, 5))
        else:
            self._tracker_boxes = np.concatenate(valid_trackers)

    def _solve_assignment_problem(self, detection_boxes, tracker_boxes):
        """
        Solve the assignment problem of matching detection bounding boxes to tracker bounding boxes using the
        Hungarian assignment algorithm (in _match_detections_to_trackers), with a box metric to calculate "distance"
        between individual detection and tracker pairs. Return indices of matched and unmatched detections and trackers.

        :param np.ndarray detection_boxes: array of detection bounding boxes.
        :param np.ndarray tracker_boxes: array of tracker bounding boxes.
        :return: numpy arrays of indices for matches (2D, pair as row) and unmatched detections and trackers (1D).
        """

        # Match detection boxes to predicted tracker boxes using the box-distance metric.
        matches, unmatched_detections, unmatched_trackers = Sort._match_detections_to_trackers(
            detection_boxes=detection_boxes,
            tracker_boxes=tracker_boxes,
            metric_function=self._metric.distance,
            threshold=0.3
        )

        # Return matches and un-matches (by indices).
        return matches, unmatched_detections, unmatched_trackers

    @staticmethod
    def _match_detections_to_trackers(detection_boxes, tracker_boxes, metric_function, threshold=0.3):
        """
        Assign detections to tracked object using the Hungarian algorithm (both represented as bounding boxes).

        :param numpy.ndarray detection_boxes: bounding boxes matrix for detections in [*corners, score] row format.
        :param numpy.ndarray tracker_boxes: bounding boxes matrix for trackers in [*corners, ID] row format.
        :param metric_function: callable that takes 2 bounding boxes and returns a non-negative real number.
        :param float threshold: metric score threshold for detection-tracker match.
        :return: a tuple of 3 lists containing matches, unmatched_detections and unmatched_trackers.
        """

        # Extract number of trackers and detections
        num_detections = detection_boxes.shape[0]
        num_trackers = tracker_boxes.shape[0]

        # Return empty if no trackers are active.
        if num_trackers == 0:
            return np.empty((0, 2), dtype=int), \
                   np.arange(num_detections), \
                   np.empty((0, 5), dtype=int)

        # Instantiate metric score matrix.
        score_matrix = np.zeros((num_detections, num_trackers), dtype=np.float32)

        # Fill metric score matrix using the static distance method of the Metric interface.
        for d_index, d_box in enumerate(detection_boxes):
            for t_index, t_box in enumerate(tracker_boxes):
                score_matrix[d_index, t_index] = metric_function(d_box, t_box)

        # Match trackers to detections using an optimized scipy implementation of the Hungarian algorithm.
        matched_indices = np.array(linear_sum_assignment(-score_matrix)).T

        # List all unmatched detections.
        unmatched_detections = []
        for d_index, d_box in enumerate(detection_boxes):
            if d_index not in matched_indices[:, 0]:
                unmatched_detections.append(d_index)

        # List all unmatched trackers.
        unmatched_trackers = []
        for t_index, t_box in enumerate(tracker_boxes):
            if t_index not in matched_indices[:, 1]:
                unmatched_trackers.append(t_index)

        # Filter out detection-tracker matches with a low metric score (under the threshold).
        matches = []
        for match in matched_indices:
            if score_matrix[match[0], match[1]] < threshold:
                unmatched_detections.append(match[0])
                unmatched_trackers.append(match[1])
            else:
                matches.append(match.reshape(1, 2))

        # Convert list of matches to numpy array.
        if not matches:
            matches = np.empty((0, 2), dtype=int)
        else:
            matches = np.concatenate(matches, axis=0)

        # Return matches and un-matches as numpy arrays.
        return matches, np.array(unmatched_detections), np.array(unmatched_trackers)

    @staticmethod
    def report():
        """
        Return a statement about how much time was spend updating state (tracking with the _update method).

        :return str: report on execution time and frames processed per second.
        """
        return f"Total Tracking took: {TimerUtility.total_time:.3f} "\
               f"for {TimerUtility.total_count} frames "\
               f"or {TimerUtility.total_count / TimerUtility.total_time:.1f} FPS."


class SimilaritySort(Sort, ABC):
    """
    Implements a Simple Online Realtime Tracking (SORT) multiple object tracker with a cascaded similarity metric.

    Unlike the original SORT, to solve the assignment problem this implementation uses both a box-boundary-only
    metric (like IOU) and an appearance-based similarity metric for the crops associated with detection/tracker
    bounding boxes. To eliminate access memory consumption crops are "saved" as slices (views) to current loaded
    frame. To maintain real-time performance assignment is solved in two cascaded iterations. First, where a score
    matrix is calculated using the (faster) box-boundary-only metric. Second, where the score matrix is calculated
    using the (slower) similarity metric, applied only to unmatched detections and trackers. High runtime speed is
    maintained (260+ FPS) since (for MOT15 data) the number of unmatched trackers/detections per frame is small (<=5).
    """

    def __init__(self, similarity_first=False, cascade=True, min_score=0.5, *args, **kwargs):
        """
        Initialize a similarity SORT object.

        :param bool similarity_first: if True similarity score is used in the first (not second) round in the cascade.
        :param bool cascade: if True metrics are cascaded (both 1st and 2nd rounds are performed) otherwise only 1st.
        :param int min_score: the minimal score threshold to keep matches based on similarity score.
        """

        # Pass SORT arguments to superclass.
        super().__init__(*args, **kwargs)

        # Save SimilaritySORT parameters.
        self.similarity_first = similarity_first
        self.cascade = cascade

        # Save keep-threshold for similarity-based match.
        self.min_score = min_score

    def _solve_assignment_problem(self, detection_boxes, tracker_boxes):
        """
        Solve the assignment problem of matching detection bounding boxes to tracker bounding boxes using the
        Hungarian assignment algorithm (in _match_detections_to_trackers), with metric functions to calculate
        "distance" between individual detection and tracker pairs. Return indices of matched and unmatched detections
        and trackers. The assignment is done in two cascaded steps, each uses the Hungarian assignment algorithm with
        a score matrix calculated using a different metric. By default, the first metric is the main box metric (e.g.,
        IOU) which only takes into account the box location. The second metric is a similarity score based on the
        appearance of crops associated with each box in the current frame. The cascading is done such that unmatched
        detections and trackers after the first round continue to a second round using the second metric,
        and the second round matched are added to the first round matches.

        :param np.ndarray detection_boxes: array of detection bounding boxes.
        :param np.ndarray tracker_boxes: array of tracker bounding boxes.
        :return: numpy arrays of indices for matches (2D, pair as row) and unmatched detections and trackers (1D).
        """

        # Default case: 1st round uses a (non-appearance-based) box metric and 2nd round uses similarity between crops.
        metrics = [self._metric.distance, self.similarity]
        thresholds = [0.3, self.min_score]

        # If similarity is flagged as "main" (first) metric switch the order.
        if self.similarity_first:
            metrics = list(reversed(metrics))
            thresholds = list(reversed(thresholds))

        # First round: match detection boxes to predicted tracker boxes using the first metric.
        matches, unmatched_detections, unmatched_trackers = Sort._match_detections_to_trackers(
            detection_boxes=detection_boxes,
            tracker_boxes=tracker_boxes,
            metric_function=metrics[0],
            threshold=thresholds[0]
        )

        # Second round (if cascade flag is True): match unmatched detections and trackers using the second metric.
        if self.cascade and unmatched_detections.size > 0 and unmatched_trackers.size > 0:

            # Match with a score matrix calculated using second metric.
            new_matches, still_unmatched_detections, still_unmatched_trackers = Sort._match_detections_to_trackers(
                detection_boxes=detection_boxes[unmatched_detections, :],
                tracker_boxes=tracker_boxes[unmatched_trackers, :],
                metric_function=metrics[1],
                threshold=thresholds[1]
            )

            # Convert new match indices associated with unmatched arrays to indices of detection_boxes / tracker_boxes.
            new_matches = np.concatenate((
                unmatched_detections[new_matches[:, 0]].reshape(-1, 1),
                unmatched_trackers[new_matches[:, 1]].reshape(-1, 1)
            ), axis=1)

            # Keep track of the total tracker-drop saves the second round provided throughout the feed.
            if new_matches.size > 0:
                self._tracker_saves += new_matches.shape[0]

            # Append second-round matches to first-round matches.
            matches = np.concatenate((matches, new_matches), axis=0)

            # Remove indices associated with newly matched detections from arrays denoting unmatched detections.
            if still_unmatched_detections.size > 0:
                unmatched_detections = unmatched_detections[still_unmatched_detections]
            else:
                unmatched_detections = np.empty((0,), dtype=int)

            # Remove indices associated with newly matched trackers from arrays denoting unmatched trackers.
            if still_unmatched_trackers.size > 0:
                unmatched_trackers = unmatched_trackers[still_unmatched_trackers]
            else:
                unmatched_trackers = np.empty((0,), dtype=int)

        # Return combined matches for both rounds and the remaining un-matches (by indices).
        return matches, unmatched_detections, unmatched_trackers

    @abstractmethod
    def similarity(self, box1, box2):
        """
        Calculate a similarity score between box1 and box2 based on the their corresponding crops in the current frame.

        :param np.ndarray box1: vector representing a bounding box in corners format [x1, y1, x2, y2].
        :param np.ndarray box2: vector representing a bounding box in corners format [x1, y1, x2, y2].
        :return float: similarity score between the image crops associated with input boxes for current frame.
        """
        pass

    def crop_current_frame(self, box):
        """
        Return a crop as a slice of the current frame defined by a bounding box. Not a copy but a view into the frame.

        :param np.ndarray box: vector representing a bounding box in corners format [x1, y1, x2, y2].
        :return np.ndarray: crop as a slice of the current frame defined by the input bounding box (not a copy).
        """

        # Calculate box width and height.
        x = box[0]
        y = box[1]
        w = box[2] - box[0]
        h = box[3] - box[1]

        # Get the pixel range for this crop (as integers). Ensure crop is contained within current frame limits.
        x1 = min(max(int(x), 0), self._current_frame.shape[1])
        y1 = min(max(int(y), 0), self._current_frame.shape[0])
        x2 = min(int(x + w), self._current_frame.shape[1])
        y2 = min(int(y + h), self._current_frame.shape[0])

        # Return a slice of the current frame as crop (NOT a copy so as not to bloat memory consumption).
        return self._current_frame[y1:y2, x1:x2]

    def run(self, *args, **kwargs):
        """
        Run the multiple object tracker instance on the input feed by iteration using the superclass run method and
        at the end print a report that states the number of tracker drops saved by the cascaded metric.
        """
        super().run(*args, **kwargs)
        print(f"Processing {self._feed.name} complete: {self._tracker_saves} drops saved by cascaded metric.")


class ClassicSimilaritySort(SimilaritySort):
    """
    Implements a Simple Online Realtime Tracking (SORT) multiple object tracker with a cascaded similarity metric.

    The similarity score for two image crops is calculated in four steps as follows:
    (1) Find keypoints and descriptors for each crop using SIFT (scale invariance is an issue since crops are small).
    (2) Count "good" matches using a nearest-neighbor distance ratio test to eliminate ambiguous matches.
    (3) Score the match between crops by counting the number of good matches
    (4) Normalize the score to the range 0 to 1 using a nonlinear function that transitions around 2-3 good matches.

    NOTE: Cascading a similarity metric to catch unmatched trackers/detections works well with a higher max_age.
    """

    def __init__(self, normalize_score=True, parity_count=3, *args, **kwargs):
        """
        Initialize a classic similarity SORT object.

        :param bool normalize_score: if True similarity score is normalized to value in the range 0 to 1.
        :param int parity_count: the number of good keypoint matches needed for a normalized score of 0.5.
        """

        # Pass similarity SORT arguments to superclass.
        super().__init__(*args, **kwargs)

        # Save the normalize score flag.
        self.normalize_score = normalize_score

        # Set the number of good keypoint matches needed for a normalized score of 0.5.
        self._match_count_to_parity = parity_count

        # Show matches/mismatches for cascaded assignment round for visual inspection (set True only for testing).
        self._observe_matches = False
        self._observe_mismatches = False

    def similarity(self, box1, box2):
        """
        Calculate a similarity score between box1 and box2 based on the appearance of their corresponding crops in
        the current frame. Appearance is inferred from detecting corner-ish features (keypoints) using the SIFT
        algorithm and counting good matches between pairs of keypoints subject to a 2-nearest-neighbors distance
        ratio test. Score can optionally be normalized to values between 0 (no matches) and 1 (many matches).

        :param np.ndarray box1: "query" vector representing a bounding box in corners format [x1, y1, x2, y2].
        :param np.ndarray box2: "train" vector representing a bounding box in corners format [x1, y1, x2, y2].
        :return float: similarity score between the image crops associated with input boxes for current frame.
        """

        # Get crop slices corresponding to both boxes from current frame.
        crop1 = self.crop_current_frame(box1)
        crop2 = self.crop_current_frame(box2)

        # If any one of the crops is empty return zero similarity (no match).
        if crop1.size == 0 or crop2.size == 0:
            return 0

        # Create grayscale temporal copies of the crops.
        crop1_gray = cv2.cvtColor(crop1, cv2.COLOR_BGR2GRAY)
        crop2_gray = cv2.cvtColor(crop2, cv2.COLOR_BGR2GRAY)

        # Detect keypoints in both grayscale crops using SIFT ("good" features).
        detector = cv2.SIFT_create()
        kp1, descriptors1 = detector.detectAndCompute(crop1_gray, None)
        kp2, descriptors2 = detector.detectAndCompute(crop2_gray, None)

        # If for either crops there are less than 2 keypoints return zero similarity (no match).
        if len(kp1) < 2 or len(kp2) < 2:
            # It also avoids exception when trying to match to no keypoints or to ratio-test 2nn-matches for 1 keypoint.
            return 0

        # Match "query" keypoints to 2 nearest-neighbors keypoints in "train" (filtered to 1 in upcoming ratio test).
        bf = cv2.BFMatcher()
        kp_matches = bf.knnMatch(descriptors1, descriptors2, k=2)

        # Perform a ratio test to eliminate matches that are ambiguous (ratio between 2 nn-matched close to 1).
        threshold = 0.75
        passing_kp_matches = [[m] for m, n in kp_matches if m.distance < threshold * n.distance]

        # If there are any plotters draw the matched keypoints over the current (sliced) frame as green circles.
        if self._plotters:
            kp1_matched = [kp1[match[0].queryIdx] for match in passing_kp_matches]
            cv2.drawKeypoints(crop1, kp1_matched, outImage=crop1, color=(0, 255, 0), flags=0)

        # Score based on the number of matches.
        score = len(passing_kp_matches)

        # Normalize score to the range between 0 and 1.
        if self.normalize_score:
            score = ClassicSimilaritySort.normalize_match_count(
                number_of_matches=score,
                match_count_to_parity=self._match_count_to_parity
            )

        # Observe matches or mismatches for testing purposes if flags are raised.
        if (self._observe_matches and score >= self.min_score) or (self._observe_mismatches and score < self.min_score):
            self._show_crops_and_keypoint_matches(box1, box2, crop1, crop2, kp1, kp2, passing_kp_matches)

        # Return similarity score between appearance of crops corresponding to boxes 1 and 2 in the current frame.
        return score

    def _show_crops_and_keypoint_matches(self, box1, box2, crop1, crop2, kp1, kp2, passing_kp_matches):
        """
        Show the input crops and corresponding keypoints and matches (for testing purposes).

        :param np.ndarray box1: "query" vector representing a bounding box in corners format [x1, y1, x2, y2].
        :param np.ndarray box2: "train" vector representing a bounding box in corners format [x1, y1, x2, y2].
        :param crop1: image crop as a slice from current frame corresponding to box1.
        :param crop2: image crop as a slice from current frame corresponding to box2.
        :param kp1: keypoints detected for crop1 (output from SIFT detect).
        :param kp2: keypoints detected for crop2 (output from SIFT detect).
        :param passing_kp_matches: keypoints matches between crop1 and crop2 (output from SIFT knn match).
        """

        # Creates joined image with keypoints drawn.
        image_joined = cv2.drawMatchesKnn(
            cv2.drawKeypoints(crop1, kp1, outImage=None, color=(0, 255, 0), flags=0),
            kp1,
            cv2.drawKeypoints(crop2, kp2, outImage=None, color=(0, 255, 0), flags=0),
            kp2,
            passing_kp_matches,
            flags=2,
            outImg=None
        )

        # Show each crop and the matches.
        cv2.imshow("crop1", crop1)
        cv2.imshow("crop2", crop2)
        cv2.imshow("joined", image_joined)

        # Save to file if user presses 's'.
        if cv2.waitKey() == ord("s"):
            path = f"{OutputParser.get_base_path()}/crops"
            s_box1 = f"box_{int(box1[0])}_{int(box1[1])}_{int(box1[2])}_{int(box1[3])}"
            s_box2 = f"box_{int(box2[0])}_{int(box2[1])}_{int(box2[2])}_{int(box2[3])}"
            s_frame = f"{self._feed.name}_fn_{self._feed.index + 1}"
            cv2.imwrite(f"{path}/{s_frame}_{s_box1}.png", crop1)
            cv2.imwrite(f"{path}/{s_frame}_{s_box2}.png", crop2)
            cv2.imwrite(f"{path}/{s_frame}_{s_box1}_{s_box2}.png", image_joined)

        # Destroy windows this method created.
        cv2.destroyWindow("crop1")
        cv2.destroyWindow("crop2")
        cv2.destroyWindow("joined")

    @staticmethod
    def normalize_match_count(number_of_matches, match_count_to_parity):
        """
        Calculate a normalized score for the input match count such that the score is between 0 and 1 and and is equal
        to 0.5 (parity) when the number_of_matches equals the match_count_to_parity input.

        :param int number_of_matches: number of matches.
        :param int match_count_to_parity: matches count for which normalized score should be 0.5.
        :return float: normalized score between 0 and 1 corresponding to number_of_matches.
        """
        count_ratio = number_of_matches / match_count_to_parity
        return count_ratio / (1.0 + count_ratio)
