import cv2
import numpy as np
from abc import ABC, abstractmethod
from parsers import OutputParser


class Plotter(ABC):
    """Plotter for images with ID-ed bounding boxes drawn on them with persistent color per box-ID."""

    # Persistent random box coloring scheme.
    _colors = 255 * np.random.rand(32, 3)

    def __init__(self):
        """Initialize current image placeholder."""
        self._this_image = None

    @abstractmethod
    def process_start(self):
        """Perform initialization tasks."""
        pass

    def process_next(self, image, boxes=None):
        """
        Plot the current input image and its associated bounding boxes. Alert caller if execution was interrupted.

        :param np.ndarray image: current image to plot.
        :param np.ndarray boxes: boxes to draw over the image.
        :return bool: flag True if execution was normal and False if execution was interrupted.
        """

        # Save this current image.
        self._this_image = image

        # Draw tracker bounding boxes on this frame if provided.
        if isinstance(boxes, np.ndarray):
            self._draw_boxes(boxes)

        # Plot this image and inform client if execution was interrupted.
        return self._plot_this_image()

    @abstractmethod
    def process_stop(self):
        """Perform finalization tasks."""
        pass

    def _draw_boxes(self, boxes):
        """
        Iteratively draw bounding boxes on current image.

        :param np.ndarray boxes: boxes to draw on current image.
        """
        for box in boxes:
            # Unpack bounding box and its id.
            *box, box_id = box.astype(np.int32)

            # Draw box on current image and annotate with its ID.
            self._draw_box(
                box=box,
                box_id=box_id,
                color=self._pick_color(box_id)
            )

    def _draw_box(self, box, box_id, color):
        """
        Draw one box on current image with given color box ID annotation.

        :param np.ndarray box: a box in the upper-left lower-right corners format.
        :param int box_id: the ID to annotate the box with (in white).
        :param np.ndarray color: BGR color to use for the box lines (values in 0-255).
        """

        # Unpack box to corner pixels (1: upper left, 2: lower right).
        x1, y1, x2, y2 = box

        # Draw a bounding box (tracker state) onto this frame.
        self._this_image = cv2.rectangle(
            self._this_image,
            (x1, y1),  # Upper left corner.
            (x2, y2),  # Lower right corner.
            color=color,
            thickness=2
        )

        # Annotate each box with its ID (just above bounding box).
        cv2.putText(
            self._this_image,
            str(box_id),
            (int(x1), int(y1)),
            fontFace=cv2.FONT_HERSHEY_DUPLEX,
            fontScale=1,
            color=(255, 255, 255),
            lineType=2
        )

    @classmethod
    def _pick_color(cls, box_id):
        """
        Pick persistent color for a given box ID.

        :param int box_id: integer id to be mapped to a color.
        :return np.ndarray: a BGR color given by 3 integers in the format [0-255, 0-255, 0-255].
        """
        return cls._colors[box_id % 32, :]

    @abstractmethod
    def _plot_this_image(self):
        """Plot current image."""
        pass


class DisplayPlotter(Plotter):
    """Plotter that displays images on screen with ID-ed bounding boxes drawn with persistent color per box-ID."""

    # Instance counter used for default window name.
    _id = 0

    def __init__(self):
        """Initialize current image placeholder and default window name using instance ID."""
        super().__init__()
        DisplayPlotter._id += 1  # Default window ID starts from 1.
        self._window_name = f"Window {self._id}"

    def process_start(self, window_name=None):
        """
        Initialize plotter by creating a new named window.

        :param str window_name: name to give to plotter window.
        """
        if window_name:
            self._window_name = window_name
        cv2.namedWindow(self._window_name)
        cv2.resizeWindow(self._window_name, 400, 300)
        cv2.moveWindow(self._window_name, 500, 500)

    def process_stop(self):
        """Finalize plotter by destroying its named window."""
        cv2.destroyWindow(self._window_name)

    def _plot_this_image(self):
        """
        Show current frame to window.

        :return bool: False if user pressed the quit keystroke ('q') True otherwise.
        """
        cv2.imshow(self._window_name, self._this_image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            return False  # Execution interrupted.
        return True  # Normal execution.


class VideoPlotter(Plotter):

    # Instance counter used for default saved video file name.
    _id = 0

    def __init__(self):
        """Initialize current image placeholder and default window name using instance ID."""
        super().__init__()
        VideoPlotter._id += 1  # Default saved video filename starts from 1.
        self._name = f"video_{self._id}"
        self._video_writer = None
        self._frame_size = None
        self._frames_per_second = 10  # FIXME set to frame rate of source video file.
        self._four_cc_code = cv2.VideoWriter_fourcc(*"XVID")

    def process_start(self, name=None):
        """
        Initialize plotter by creating a new named video writer.

        :param str name: name to give to saved video file.
        """

        # Save video filename if given.
        if name:
            self._name = name

        # Do not instantiate video writer before a first frame is provided and the frame size is extracted.
        if not self._frame_size:
            return

        # Instantiate a video writer object once the frame size parameter is saved (process_start is called a 2nd time).
        self._video_writer = cv2.VideoWriter(
            OutputParser.get_video_filename(sequence=self._name),
            self._four_cc_code,
            self._frames_per_second,
            self._frame_size
        )

    def process_stop(self):
        """Finalize plotter by releasing the video writer."""
        self._video_writer.release()

    def _plot_this_image(self):
        """
        Write current frame to video file.

        :return bool: Always return True.
        """

        # Save the frame size and recall process_start to instantiate video writer if this is the first frame.
        if not self._frame_size:
            height, width = tuple(self._this_image.shape[:2])
            self._frame_size = (width, height)
            self.process_start()

        # Write current frame to video file.
        self._video_writer.write(self._this_image)
        return True  # Normal execution.
