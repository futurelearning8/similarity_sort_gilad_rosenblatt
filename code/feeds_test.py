import unittest
import cv2
import numpy as np
from feeds import FileFeed, StubFeed
from parsers import DataParser


class TestFileFeed(unittest.TestCase):
    """Test the feeders.FileFeed class."""

    # A list of all sequence names.
    _sequences = DataParser.train_sequences + DataParser.test_sequences

    def test_feed(self):
        """Test the frame numbers the file feed generates."""

        # Test for all sequences.
        for sequence in self._sequences:

            # Generate list of filenames
            number_of_frames = DataParser.get_number_of_frames(sequence)
            filenames = (DataParser.get_frame_filename(sequence, index + 1) for index in range(number_of_frames))

            # Initialize a file feed that generates actual images.
            feed = FileFeed(
                name=sequence,
                number_of_frames=number_of_frames,
                filenames=filenames
            )

            # Assert feed returns correct frames for the first 5 frames.
            for index, state in enumerate(feed):
                if not index < 5:
                    break
                self.assertTrue(np.all(cv2.imread(DataParser.get_frame_filename(sequence, index + 1)) == state["frame"]))


class TestStubFeed(unittest.TestCase):
    """Test the feeders.StubFeed class."""

    # A list of all sequence names.
    _sequences = DataParser.train_sequences + DataParser.test_sequences

    def test_feed(self):
        """Test the stub images (running frame numbers) the file feed generates."""

        # Test for all sequences.
        for sequence in self._sequences:

            # Initialize a file feed that only generates frame numbers (not the actual images).
            feed = StubFeed(
                name=sequence,
                number_of_frames=DataParser.get_number_of_frames(sequence)
            )

            # Assert feed returns correct frame numbers for the first 5 frames.
            for index, state in enumerate(feed):
                if not index < 5:
                    break
                self.assertTrue(index + 1 == state["frame"])


if __name__ == "__main__":
    unittest.main()
