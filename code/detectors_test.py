import unittest
import numpy as np
from detectors import StubDetector
from parsers import DataParser


class TestStubDetector(unittest.TestCase):
    """Test the detectors.StubDetector class."""

    # A list of all sequence names.
    _sequences = DataParser.train_sequences + DataParser.test_sequences

    def test_stub_detect(self):
        """Test the bounding boxes for each frame."""

        # Test for all sequences.
        for sequence in self._sequences:

            # Create generator for pre-detected bounding boxes for this image sequence.
            number_of_frames = DataParser.get_number_of_frames(sequence)
            detections = DataParser.get_detections(sequence)
            boxes = (DataParser.extract_boxes(detections, frame_number=index + 1) for index in range(number_of_frames))

            # Initialize a stub detector that generates bounding boxes from cached boxes generator.
            detector = StubDetector(boxes_generator=boxes)

            # Assert detector returns correct bounding boxes.
            for index in range(number_of_frames):
                bounding_boxes_actual = detector.detect(image=index + 1)
                bounding_boxes_expected = DataParser.extract_boxes(detections=detections, frame_number=index + 1)
                self.assertEqual(np.all(bounding_boxes_actual), np.all(bounding_boxes_expected))


if __name__ == "__main__":
    unittest.main()
