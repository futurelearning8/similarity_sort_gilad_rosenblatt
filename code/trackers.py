import numpy as np
from abc import ABC, abstractmethod
from filterpy.kalman import KalmanFilter
from boxops import BoxConvertor


class BoxTracker(ABC):
    """Tracks one object using an id-ed bounding box."""

    # Unique instance ID.
    _instance_count = 0

    def __init__(self):
        """Reset tracker state parameters and issue unique instance ID."""

        # Initialize state.
        self.time_since_update = 0
        self.hits = 0
        self.hit_streak = 0
        self.age = 0

        # Issue unique ID to this tracker instance.
        self._id = KalmanBoxTracker._issue_id()

    @abstractmethod
    def update(self, box):
        """Update the state vector with observed bounding box."""
        pass

    @abstractmethod
    def predict(self):
        """Advance the state vector and return the predicted bounding box estimate."""
        pass

    @property
    @abstractmethod
    def box(self):
        """
        Return the current bounding box estimate in the corners representation [x1, y1, x2, y2],
        where x1, y1 is the top left and x2, y2 is the bottom right.
        """
        pass

    @property
    def id(self):
        """Return the unique id of this tracker class instance."""
        return self._id

    @classmethod
    def _issue_id(cls):
        """Assign unique ID by incrementing class instance counter."""
        this_id = cls._instance_count
        cls._instance_count += 1
        return this_id


class KalmanBoxTracker(BoxTracker):
    """Implements a BoxTracker abstract class using a Kalman filter."""

    def __init__(self, box):
        """
        Initialize a Kalman filter tracker using an bounding box.

        :param np.ndarray box: a bounding box in corners format [x1, y1, x2, y2] used to initialize the Kalman filter.
        """

        # Reset tracker state and issue ID.
        super(KalmanBoxTracker, self).__init__()

        # Define a constant velocity model to fit bounding box using a Kalman filter (with custom settings).
        self.kf = KalmanBoxTracker._initialize_custom_filter(box)

        # Instantiate list of consecutive tracked bounding boxes.
        self.history = []

    def update(self, box):
        """
        Update the state vector with observed bounding box.

        :param np.ndarray box: a bounding box in corners format [x1, y1, x2, y2] used to update the Kalman filter.
        """
        self.time_since_update = 0
        self.history = []
        self.hits += 1
        self.hit_streak += 1
        self.kf.update(BoxConvertor.to_centroid(box, column_output=True))

    def predict(self):
        """Advance the state vector and return the predicted bounding box estimate."""
        if (self.kf.x[6] + self.kf.x[2]) <= 0:
            self.kf.x[6] *= 0.0
        self.kf.predict()
        self.age += 1
        if self.time_since_update > 0:
            self.hit_streak = 0
        self.time_since_update += 1
        self.history.append(self.box)
        return self.history[-1]

    @property
    def box(self):
        """Return the current bounding box estimate."""
        return BoxConvertor.from_centroid(self.kf.x)

    @staticmethod
    def _initialize_custom_filter(box):
        """
        Initialize and configure a Kalman filter and initialize it using a bounding box.

        :param np.ndarray box: a bounding box in corners format [x1, y1, x2, y2] used to initialize the Kalman filter.
        """

        # Instantiate a Kalman filter.
        kalman_filter = KalmanFilter(dim_x=7, dim_z=4)

        # Define the state transition matrix.
        kalman_filter.F = np.array([
            [1, 0, 0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0, 1, 0],
            [0, 0, 1, 0, 0, 0, 1],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 1]
        ])

        # Define the measurement function.
        kalman_filter.H = np.array([
            [1, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0]
        ])

        # Define the covariance matrix.
        kalman_filter.P[4:, 4:] *= 1000.0  # Give high uncertainty to unobservable initial velocities.
        kalman_filter.P *= 10.0

        # Assign measurement noise.
        kalman_filter.R[2:, 2:] *= 10.0

        # Assign process noise.
        kalman_filter.Q[-1, -1] *= 0.01
        kalman_filter.Q[4:, 4:] *= 0.01

        # Assign initial value to input bounding box.
        kalman_filter.x[:4] = BoxConvertor.to_centroid(box, column_output=True)

        # Return configured Kalman filter.
        return kalman_filter
