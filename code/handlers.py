from abc import ABC, abstractmethod
from mots import Sort, ClassicSimilaritySort
from mots_deep import DeepSimilaritySort
from parsers import DataParser, VideoParser, OutputParser
from feeds import FileFeed, StubFeed, VideoFeed, FeedIteratorWithCallbacks
from detectors import StubDetector, Detector


class Configuration:
    """Configuration flags container forExecutionHandler initialization."""

    def __init__(self, display=False, write_video=False, write_benchmark=True, stub_feed=False, from_video=False):
        """
        Initializes an execution configuration for running multiple-object detection.

        :param bool display: if True output of the multiple-object tracking process is showed on screen.
        :param bool write_video: if True output of the multiple-object tracking process is written to a video file.
        :param bool write_benchmark: if True benchmark output of the multiple-object tracking is written to a text file.
        :param bool stub_feed: if True the multiple-object tracker feeds on frame numbers without loading images.
        :param bool from_video: if True the multiple-object tracker feeds on frame loaded from a raw video file.
        """

        # Assert flag combination is logically consistent (e.g., if no images are loaded none are required).
        if stub_feed:
            assert not from_video and not display and not write_video

        # Save execution flags.
        self.display = display
        self.write_video = write_video
        self.write_benchmark = write_benchmark
        self.from_video = from_video
        self.stub_feed = stub_feed


class ExecutionHandler(ABC):
    """Handler to configure and run multiple-object detection (interface) on image sequences."""

    def __init__(self, configuration):
        """
        Initializes an execution handler to configure and run multiple-object detection on named image sequences.

        :param Configuration configuration: a container that stores execution configuration.
        """

        # Save execution configuration flags.
        self.display = configuration.display
        self.write_video = configuration.write_video
        self.write_benchmark = configuration.write_benchmark
        self.from_video = configuration.from_video
        self.stub_feed = configuration.stub_feed

        # Set the sequence names for multi-object tracking to be executed on (iteratively).
        if not self.from_video:
            self.sequences = DataParser.train_sequences
        else:
            self.sequences = VideoParser.train_sequences

        # Instantiate hyperparameter dictionary.
        self.hyperparameters = {}

    def execute(self):
        """Run multi-object tracking using (similarity) SORT on "pre-detected" sequences and display/save results."""

        # Iterate over named image sequences.
        for sequence in self.sequences:

            # Configure a feed (image stream) to named image sequence.
            feed = self.create_and_configure_feed(sequence=sequence)

            # Create a detector (a generator of detection boxes for each frame in the feed).
            detector = self.create_and_configure_detector(sequence=sequence)

            # Instantiate a new multiple-object tracker configured to follow the feed and detector output.
            mo_tracker = self.create_and_configure_mot(feed=feed, detector=detector)

            # Run multiple object tracker on feed to process sequence (until feed iterator is terminated/exhausted).
            mo_tracker.run(OutputParser.get_data_filename(sequence))

        # Print "real-time" performance report: the total time spend "tracking" and the frames processed per second.
        print(Sort.report())  # FIXME decorating update method of mot with @jit should be done on the fly here.
        if self.display:
            print("NOTE: To get indicative runtime run without the --display flag.")

        # Write a readme file with execution parameters if any outputs are written to file.
        if self.write_benchmark or self.write_video:
            self._write_execution_parameters_to_file()

    def create_and_configure_feed(self, sequence):
        """
        Create and configure a feed iterator to named image sequence that complies with the handler's state parameters.

        :param str sequence: name of the image sequence or raw video.
        :return FeedIteratorWithCallbacks: a Feed instance corresponding to the named image sequence or video.
        """

        # Option 1: initialize a stub feed that only generates frame numbers (not the actual images).
        if self.stub_feed:
            return StubFeed(
                name=sequence,
                number_of_frames=DataParser.get_number_of_frames(sequence)
            )

        # Option 2: initialize a feed that generates frames from raw video.
        if self.from_video:
            return VideoFeed(
                capture_this=VideoParser.get_filename(sequence),
                name=sequence
            )

        # Option 3: initialize an feed that generates images from files of named sequence (using a filename generator).
        number_of_frames = DataParser.get_number_of_frames(sequence)
        return FileFeed(
            name=sequence,
            number_of_frames=number_of_frames,
            filenames=(DataParser.get_frame_filename(sequence, index + 1) for index in range(number_of_frames))
        )

    @staticmethod
    def create_and_configure_detector(sequence):
        """
        Create a stub detector feeding from a generator of pre-detected detection boxes loaded from file.

        :param str sequence: name of the image sequence or raw video.
        :return StubDetector: a StubDetector instance that generates detection boxes for named image sequence or video.
        """
        number_of_frames = DataParser.get_number_of_frames(sequence)
        detections = DataParser.get_detections(sequence)
        boxes = (DataParser.extract_boxes(detections, frame_number=index + 1) for index in range(number_of_frames))
        return StubDetector(boxes_generator=boxes)

    def create_and_configure_mot(self, feed, detector):
        """
        Create and configure a multiple object tracker object over an image feed and a detector. Once configured,
        the multiple-object tracker is locked to an image feed and updates implicitly each time the feed's get_frame
        is called. The Detector is used to extract bounding boxes for each frame generated by the feed. The handler's
        flag configurations and mot_factory property indicate which feed, detector and multiple-object tracker to use.

        :param FeedIteratorWithCallbacks feed: a configured Feed iterator for to the named image sequence or video.
        :param Detector detector: a Detector instance that generates detection boxes each frame.
        :return: a configured multiple object tracker object.
        """

        # Define all necessary arguments for the multiple-object tracker.
        mot_kwargs = dict(
            feed=feed,
            detector=detector,
            display=self.display,
            write_video=self.write_video,
            write_benchmark=self.write_benchmark
        )

        # Instantiate and configure the multiple-object tracker (set non-similarity box metric to IOU).
        mo_tracker = self.mot_factory(**mot_kwargs, **self.hyperparameters)

        # Return configured multiple-object tracker.
        return mo_tracker

    def _write_execution_parameters_to_file(self):
        """Write hyperparameters and run stats to file at the output root folder."""
        filename = f"{OutputParser.get_base_path()}/readme.txt"
        with open(filename, 'w') as output_file:
            print(Sort.report(), file=output_file)
            for parameter, value in self.hyperparameters.items():
                print(f"{parameter},{value}", file=output_file)

    @property
    @abstractmethod
    def mot_factory(self):
        """Return a factory method for creating multiple object detection objects."""
        pass


class SortHandler(ExecutionHandler):
    """Handler to configure and run SORT multiple-object detection on image sequences."""

    def __init__(self, max_age=5, min_hits=3, *args, **kwargs):
        """
        Initializes an execution handler to configure and run SORT MOT on named image sequences.

        :param int max_age: the maximum number of frame a tracker is propagated without a match before being dropped.
        :param int min_hits: the number of consecutive frames a tracker must be matched before being considered valid.
        """
        super().__init__(*args, **kwargs)
        self.hyperparameters["min_hits"] = min_hits
        self.hyperparameters["max_age"] = max_age

    @property
    def mot_factory(self):
        return Sort


class SimilaritySortHandler(SortHandler, ABC):
    """Handler to configure and run SimilaritySort multiple-object detection on image sequences."""

    def __init__(self, similarity_first=False, cascade=True, min_score=0.5, *args, **kwargs):
        """
        Initializes an execution handler to configure and run similarity SORT MOT (interface) on named image sequences.

        :param int min_score: the minimal score threshold to keep matches based on similarity score.
        :param bool similarity_first: if True the multiple-object tracker uses similarity as the primary (1st) metric.
        :param bool cascade: if False the multiple-object tracker uses ONLY the one round (primary metric only).
        """
        super().__init__(*args, **kwargs)
        self.hyperparameters["similarity_first"] = similarity_first
        self.hyperparameters["cascade"] = cascade
        self.hyperparameters["min_score"] = min_score


class ClassicSimilaritySortHandler(SimilaritySortHandler):
    """Handler to configure and run classical similarity SORT multiple-object detection on image sequences."""

    def __init__(self, normalize_score=True, parity_count=3, *args, **kwargs):
        """
        Initializes an execution handler to configure and run a classic similarity SORT MOT on named image sequences.

        :param bool normalize_score: if True similarity score is normalized to value in the range 0 to 1.
        :param int min_score: the minimal score threshold to keep matches based on similarity score.
        :param int parity_count: the number of good keypoint matches needed for a normalized score of 0.5.
        :param bool similarity_first: if True the multiple-object tracker uses similarity as the primary (1st) metric.
        :param bool cascade: if False the multiple-object tracker uses ONLY the one round (primary metric only).
        """
        super().__init__(*args, **kwargs)
        self.hyperparameters["normalize_score"] = normalize_score
        self.hyperparameters["parity_count"] = parity_count

    @property
    def mot_factory(self):
        return ClassicSimilaritySort


class DeepSimilaritySortHandler(SimilaritySortHandler):
    """Handler to configure and run deep similarity SORT multiple-object detection on image sequences."""

    def __init__(self, *args, **kwargs):
        """Initializes an execution handler to configure and run a deep similarity SORT MOT on named image sequences."""
        super().__init__(*args, **kwargs)

    @property
    def mot_factory(self):
        return DeepSimilaritySort

