import unittest
from utils import IteratorWithCallbacks
from collections import Iterator


class TestIteratorWithCallbacks(unittest.TestCase):
    """Test the utils.IteratorWithCallbacks class."""

    # Default iterations limit value to use for input IteratorWithCallbacks.__init__.
    _limit = 100

    def test_is_iterator(self):
        """Test TestIteratorWithCallbacks implements the collections.Iterator interface."""
        self.assertTrue(issubclass(IteratorWithCallbacks, Iterator))

    def test_init(self):
        """Test __init__. """
        self.assertEqual(IteratorWithCallbacks().limit, None, "Default value of limit should be None (no limit).")
        for test_num, limit in enumerate(range(1, self._limit, self._limit // 4)):  # Number of tests at most 5.
            self.assertEqual(IteratorWithCallbacks(limit=limit).limit, limit)

    def test_iter(self):
        """Test __iter__."""
        iter_wc = IteratorWithCallbacks(limit=self._limit)
        self.assertEqual(iter(iter_wc), iter_wc, "Method __iter__ must return self.")  # State initialized.
        self.assertEqual(iter_wc.index, -1, "Iteration index should be set to -1 at __iter__.")

    def test_next(self):
        """Test __next__."""

        # Manually iterate the iterator till the iteration number limit.
        iter_wc = iter(IteratorWithCallbacks(limit=self._limit))
        for _ in range(self._limit):
            next(iter_wc)

        # Assert that another call to next raises StopIteration (iterator is exhausted).
        with self.assertRaises(StopIteration):
            next(iter_wc)

    def test_index(self):
        """
        Test that index maintained as state key "index" works properly. State key "index" is the 0-based index of the
        most recent iteration. It is added to the state dictionary by IteratorWithCallbacks.__init__. Iterator is
        exhausted once index reaches _limit.
        """
        iter_wc = IteratorWithCallbacks(limit=self._limit)
        for index, state in enumerate(iter_wc):
            self.assertTrue(isinstance(state, dict))
            self.assertTrue("index" in state)
            self.assertEqual(state["index"], iter_wc.get_state("index"))
            self.assertEqual(state["index"], iter_wc.index)
            self.assertEqual(state["index"], index)
        self.assertEqual(iter_wc.index + 1, iter_wc.limit)  # Test termination condition.

    def test_exhausted(self):
        """Test the exhausted flag status before and after iterator is exhausted."""
        iter_wc = IteratorWithCallbacks(limit=self._limit)
        for _ in range(2):
            for _ in iter_wc:
                self.assertFalse(iter_wc.is_terminated)
            self.assertTrue(iter_wc.is_terminated)

    def test_terminate(self):
        """Test the terminate method."""
        iter_wc = IteratorWithCallbacks(limit=self._limit)
        for _ in iter_wc:
            self.assertFalse(iter_wc.is_terminated)
        pass

    def test_add_state(self):
        """Test add_state which adds state parameters (keys, initialization values, and update methods)."""

        # Define an iterator w/callbacks and add state parameter that increments each iteration.
        iter_wc = IteratorWithCallbacks(
            limit=self._limit
        ).add_state(
            key="test",
            init_value=0,
            update_method=lambda x: x + 1  # init=0, next=1, 2, 3, 4, ..., [iteration_number], ...
        )

        # Build two generators: expected and actual value.
        actual_generator = (state["test"] for state in iter_wc)
        expected_generator = (x + 1 for x in range(self._limit))

        # Test all generated values up to limit.
        for _ in range(self._limit):
            self.assertEqual(next(actual_generator), next(expected_generator))

    def test_add_listener_to_start(self):
        """Test add_listener_to_start which adds listener callbacks to iterator."""

        # Global state parameters for start iterations callback to side-effect on.
        nonlocal_start_iterations = 0

        # A testing callback that increments nonlocal_start_iterations.
        def my_start_callback():
            nonlocal nonlocal_start_iterations
            nonlocal_start_iterations += 1

        # Define an iterator w/callbacks and add a my_start_callback to be executed at start of iterations.
        iter_wc = IteratorWithCallbacks(
            limit=self._limit
        ).add_listener_to_start(
            callback=my_start_callback
        )

        # Check updates to nonlocal_start_iterations till iterator is exhausted.
        self.assertEqual(nonlocal_start_iterations, 0)
        for _ in iter_wc:
            self.assertEqual(nonlocal_start_iterations, 1)
        self.assertEqual(nonlocal_start_iterations, 1)

    def test_add_listener_to_next(self):
        """Test add_listener_to_next which adds listener callbacks to iterator."""

        # Global state parameters for callbacks to side-effect on.
        nonlocal_state_parameter = 0

        # A testing callback that increments nonlocal_state_parameter (must have only keyword arguments).
        def my_callback(index=None):
            nonlocal nonlocal_state_parameter
            nonlocal_state_parameter += 1

        # Define an iterator w/callbacks and add a my_callback to be executed at each iteration.
        iter_wc = IteratorWithCallbacks(
            limit=self._limit
        ).add_listener_to_next(
            callback=my_callback,
            keys=["index"]
        )

        # Check updates to nonlocal_state_parameter at each iteration till iterator is exhausted.
        for this_index, _ in enumerate(iter_wc):
            self.assertEqual(nonlocal_state_parameter, this_index + 1)  # next=1, 2, ..., [iteration_number], ...

    def test_add_listener_to_stop(self):
        """Test add_listener_to_stop which adds listener callbacks to iterator."""

        # Global state parameters for stop iterations callback to side-effect on.
        nonlocal_stop_iterations = 0

        # A testing callback that increments nonlocal_stop_iterations.
        def my_stop_callback():
            nonlocal nonlocal_stop_iterations
            nonlocal_stop_iterations += 1

        # Define an iterator w/callbacks and add a my_stop_callback to be executed at stop iterations.
        iter_wc = IteratorWithCallbacks(
            limit=self._limit
        ).add_listener_to_stop(
            callback=my_stop_callback
        )

        # Check updates to nonlocal_stop_iterations till iterator is exhausted.
        self.assertEqual(nonlocal_stop_iterations, 0)
        for _ in iter_wc:
            self.assertEqual(nonlocal_stop_iterations, 0)
        self.assertTrue(nonlocal_stop_iterations, 1)

    def test_listeners_and_state(self):
        """Test add_listener_to_next by adding a callback to iterator that tracks two added state parameters."""

        # Global state parameters for callbacks to side-effect on.
        nonlocal_state_parameter = 0

        # A testing callback that increments global_state_parameter (must have only keyword arguments).
        def my_callback(sequence1=None, sequence2=None):
            nonlocal nonlocal_state_parameter
            nonlocal_state_parameter = (sequence1, sequence2)

        # Define an iterator w/callbacks and add a my_callback to be executed at each iteration.
        iter_wc = IteratorWithCallbacks(
            limit=self._limit
        ).add_state(
            key="sequence1",
            init_value=0,
            update_method=lambda x: x + 1  # init=0, next=1, 2, 3, 4, ..., [iteration_number], ...
        ).add_state(
            key="sequence2",
            init_value=1,
            update_method=lambda x: x + x  # init=1, next=2, 4, 8, 16, ..., 2^[iteration_number], ...
        ).add_listener_to_next(
            callback=my_callback,
            keys=["sequence1", "sequence2"]
        )

        # Check updates to nonlocal_state_parameter result in the expected sequences (till iterator is exhausted).
        for index, _ in enumerate(iter_wc):
            iteration_number = index + 1  # Number of iteration starts with 1.
            value1, value2 = nonlocal_state_parameter
            self.assertEqual(value1, iteration_number)
            self.assertEqual(value2, 2**iteration_number)


if __name__ == "__main__":
    unittest.main()
