import cv2
import tensorflow as tf
import numpy as np
from abc import ABC
from mots import SimilaritySort


class DeepSimilaritySort(SimilaritySort, ABC):
    """
    Implements a Simple Online Realtime Tracking (SORT) multiple object tracker with a cascaded deep similarity
    metric. The metric is implemented by a siamese net that implements a deep similarity metric to identify if the
    person in two images is the same. The similarity score is the probability predicted by the net.
    """

    # Trained siamese similarity model for input shape (None, 128, 64 ,6).
    RESIZE_SHAPE = (64, 128)
    model = tf.keras.models.load_model("../models/my_model_bsz64_pos32_spe124_epo40_ckp_acc0.803", custom_objects={
        "tf": tf
    })  # Workaround NameError in Lambda: <https://github.com/keras-team/keras/issues/5088#issuecomment-273851273>.

    def __init__(self, *args, **kwargs):
        """
        Initialize a deep similarity SORT object.

        :param bool normalize_score: if True similarity score is normalized to value in the range 0 to 1.
        :param int parity_count: the number of good keypoint matches needed for a normalized score of 0.5.
        """

        # Pass similarity SORT arguments to superclass.
        super().__init__(*args, **kwargs)

        # Show matches/mismatches for cascaded assignment round for visual inspection (set True only for testing).
        self._observe_matches = False
        self._observe_mismatches = False

    def similarity(self, box1, box2):
        """
        Calculate a similarity score between box1 and box2 based on the appearance of their corresponding crops in
        the current frame. Appearance is inferred from predictions of a deep siamese similarity net. The score is
        the predicted 0-1 probability that the two crops are of the same person.

        :param np.ndarray box1: "query" vector representing a bounding box in corners format [x1, y1, x2, y2].
        :param np.ndarray box2: "train" vector representing a bounding box in corners format [x1, y1, x2, y2].
        :return float: similarity score between the image crops associated with input boxes for current frame.
        """

        # Get crop slices corresponding to both boxes from current frame (BGR since frames are loaded with opencv).
        crop1 = self.crop_current_frame(box1)
        crop2 = self.crop_current_frame(box2)

        # If any one of the crops is empty return zero similarity (no match).
        if crop1.size == 0 or crop2.size == 0:
            return 0

        # Preprocess: resize crops to (width, height)=(64, 128), concatenate along the channel dimension and normalize.
        inputs = np.zeros(shape=(1, *reversed(self.RESIZE_SHAPE), 6), dtype=np.float32)
        images = tuple(cv2.resize(src=crop, dsize=DeepSimilaritySort.RESIZE_SHAPE) for crop in [crop1, crop2])
        inputs[0, :, :, :] = np.dstack(images) / 255

        # Calculate similarity (match) probability
        score = self.model.predict(np.einsum("ikjl->ijkl", inputs))[0, 0]

        # Observe matches or mismatches for testing purposes if flags are raised.
        if (self._observe_matches and score >= self.min_score) or (self._observe_mismatches and score < self.min_score):
            DeepSimilaritySort._show_resized_crops(images)

        # Return probability as similarity score between appearance of crops corresponding to boxes 1 and 2.
        return score

    @staticmethod
    def _show_resized_crops(images):
        """
        Show both resized crops side by side.

        :param tuple images: (image1, image2) where each image is has the same shape.
        """
        cv2.imshow("crops", np.hstack(images))
        cv2.waitKey()
        cv2.destroyWindow("crops")
