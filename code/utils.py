# Container for general purpose utilities. Written by Gilad Rosenblatt (2021).

import time
from collections import Iterator
from abc import ABC, abstractmethod


class TimerUtility:
    """Count cumulative runtime of a method or function using a decorator with an outer enclosing scope."""

    # Total (cumulative) time this class timed for all calls.
    total_time = 0

    # Amount of times this class method timed a call.
    total_count = 0

    @classmethod
    def cumulative_timeit(cls, decorated_function):
        """Decorator method that tracks cumulative runtime for all calls to a class method or function."""

        def wrapper(*args, **kwargs):
            start_time = time.time()
            output = decorated_function(*args, **kwargs)
            cls.total_time += time.time() - start_time
            cls.total_count += 1
            return output

        return wrapper


class IteratorWithCallbacks(Iterator):
    """
    Iterator with state that registers listener callbacks to execute at each iteration till reaching iteration limit,
    and at stop iterations. Callback functions to next iteration must have keyword arguments that correspond to a
    subset of keys of the object state dictionary. Callback functions to stop iterations must take no inputs and are
    called once just before StopIteration is raised. Inheritance from collections.Iterator is meant to enforce
    implementation of the iterator interface. This class can be used as a Mixin to add an iterator w/callbacks aspect.
    """

    def __init__(self, limit=None, *args, **kwargs):
        """Initialize list of callbacks to execute at each iteration, the state dictionary, and the iterations limit."""

        # Allow use as a Mixin: pass all other arguments forward with the method resolution order resolved by super().
        # In multiple inheritance the order in which super() resolves classes is left-to-right in the inheritance list.
        super().__init__(*args, **kwargs)

        # Limit on total number of iterations before termination condition check (_is_exhausted) evaluates to True.
        self._limit = limit

        # Initialize list of tuples with callbacks and their input keys (added by listener calls to add_listener_event).
        self._callbacks_start = []
        self._callbacks_next = []
        self._callbacks_stop = []

        # Initialize iterator state management dictionaries.
        self._state = {}
        self._init_values = {}
        self._update_methods = {}

        # Add a state parameter to count the 0-based index of iterations performed since __init__.
        self.add_state(
            key="index",
            init_value=-1,
            update_method=lambda count: count + 1
        )

        # Add a flag to indicate if iterator is exhausted (set True before StopIteration, set False at __iter__).
        self._terminate_now = False

    def __iter__(self):
        """Initialize iterator."""

        # Initialize all state parameters.
        self._initialize_state()

        # Set termination state of the iterator to False.
        self._terminate_now = False

        # Notify registered listeners that the iterator is ready to start.
        for callback in self._callbacks_start:
            callback()

        # Return iteration-ready iterator.
        return self

    def __next__(self):
        """Iterate iterator and execute callbacks."""
        if not self._is_exhausted() and not self._terminate_now:

            # Update state at each iteration.
            self._update_state()

            # Execute listener callbacks. Route each callback only to its registered keys. Use keyword input arguments.
            for callback, input_keys in self._callbacks_next:
                callback(**{key: self._state[key] for key in input_keys})

            # Return state dictionary (copy to prevent side effects by listeners).
            return self._state.copy()
        else:
            # Set termination state of the iterator to True (flag is False if iteration limit is reached).
            self._terminate_now = True

            # Notify listeners that the iterator is exhausted (limit reached) or terminated (terminate was called).
            for callback in self._callbacks_stop:
                callback()

            # End iterations.
            raise StopIteration

    def add_listener_to_start(self, callback):
        """
        Add callback function that takes no inputs to list of callbacks executed once before __iter__ returns.

        :param collections.Callable callback: a callable that takes no inputs and is called once per __iter__ call.
        """

        # Validate input is a callable that is not already in callbacks.
        assert callable(callback)
        assert callback not in self._callbacks_start

        # Add input callback to instance callbacks.
        self._callbacks_start.append(callback)

        # Support dot chaining syntax.
        return self

    def add_listener_to_next(self, callback, keys):
        """
        Add new callback function to list of callback executed at each iteration. Callback functions must accept
        inputs corresponding to unpacking the iterator state dictionary (with appropriate keyword argument names).

        :param collections.Callable callback: a callable that takes the output of _get_state() as input.
        :param list keys: keys that this callback will be registered to (receive as keyword inputs at each iteration).

        NOTE: callback must only have keyword argument, all named by the list keys, and these names are all keys
        in the iterator state dictionary. The corresponding state parameters will be provided as kw-inputs to callback.
        """

        # Validate input is a callable that is not already in callbacks and keys are all in state dictionary.
        assert callable(callback)
        assert callback not in self._callbacks_next
        assert all(key in self._state.keys() for key in keys)

        # Add input callback to instance callbacks.
        self._callbacks_next.append((callback, keys))

        # Support dot chaining syntax.
        return self

    def add_listener_to_stop(self, callback):
        """
        Add callback function that takes no inputs to list of callbacks executed at iterator exhaustion.

        :param collections.Callable callback: a callable that takes no inputs and is called once before StopIteration.
        """

        # Validate input is a callable that is not already in callbacks.
        assert callable(callback)
        assert callback not in self._callbacks_stop

        # Add input callback to instance callbacks.
        self._callbacks_stop.append(callback)

        # Support dot chaining syntax.
        return self

    def add_state(self, key, init_value, update_method):
        """
        Add a state parameter to this iterator with its initialization value and method to update it at each iterations.
        The update_method input should be a callable that accepts one value and returns the updated value.

        :param str key: key for this state parameter in the state dictionary.
        :param init_value: default value for this key (at call to __iter__).
        :param update_method: method to update value for this key (at call to __next__).
        """

        # Validate inputs.
        assert isinstance(key, str)
        assert key not in self._state.keys()
        assert callable(update_method)

        # Create a new state parameter.
        self._state[key] = None  # Initialized only at next call to __iter__ (assigned to init_val).
        self._init_values[key] = init_value
        self._update_methods[key] = update_method

        # Support dot chaining syntax.
        return self

    def get_state(self, key):
        """
        Return current state for key (read only).

        :param str key: key for this state parameter in the state dictionary.
        """
        assert key in self._state.keys()
        return self._state[key]

    def _initialize_state(self):
        """
        Initialize state of iterator by setting the each state parameter value to its stored initialization value.
        """
        for key in self._state.keys():
            self._state[key] = self._init_values[key]

    def _update_state(self):
        """
        Change state of iterator by calling the stored update callable for each state parameter on current state value.
        """
        for key, value in self._state.items():
            self._state[key] = self._update_methods[key](value)

    def _is_exhausted(self):
        """Check if iterator is exhausted by reaching the limit number of iterations: index + 1 == limit (if set)."""
        return False if not self.limit else not self.index + 1 < self._limit

    @property
    def limit(self):
        """
        Return the maximum number of iterations allows (default is None meaning no limit).

        :return: the limit on the number of iterations or None if no limit is enforced.
        """
        return self._limit

    @property
    def index(self):
        """
        Return the 0-based index of the most recent iteration.

        :return int: 0-based index of most recent iteration.
        """
        return self.get_state("index")

    @property
    def is_terminated(self):
        """
        Return True if iterator is exhausted (after StopIteration is raised, up until another call to __iter__).
        Also return True if terminate method was called before reaching limit number of iterations.

        :return bool: current termination state of the iterator.
        """
        return self._terminate_now

    def terminate(self):
        """Update _terminate_now such that next call to __next__ raises StopIteration."""
        self._terminate_now = True

    def is_key(self, key):
        """
        Check if an input key is in the state dictionary (a legal key to listen to).

        :param str key: a key to check against the iterator state dictionary keys.
        :return bool: True is key in state dictionary False otherwise.
        """
        return key in self._state.keys()


class SyncIteratorWithCallbacks(ABC):
    """An object that registers itself as listener to an IteratorWithCallbacks object via callbacks."""

    def sync(self, iter_wc, keys):
        """
        Register as listener to iterator to get notified of state of given keys at each iteration using callbacks.

        :param IteratorWithCallbacks iter_wc: an iterator w/callbacks.
        :param list keys: keys in the iterator state dictionary that this object gets notified about at each iteration.
        """

        # Check iter_wc is iterator w/callbacks and that all keys are in its state dictionary.
        assert isinstance(iter_wc, IteratorWithCallbacks)
        assert all(iter_wc.is_key(key=key) for key in keys)

        # Register self as listener to start-of-iteration event using _process_start as callback.
        iter_wc.add_listener_to_start(
            callback=self._process_start,
        )

        # Register self as listener to new-iteration event using _process_next as callback for input keys.
        iter_wc.add_listener_to_next(
            callback=self._process_next,
            keys=keys
        )

        # Register self as listener to end-of-iterations event using _process_stop as callback.
        iter_wc.add_listener_to_stop(
            callback=self._process_stop,
        )

    @abstractmethod
    def _process_start(self):
        """Callback to execute at __iter__."""
        pass

    @abstractmethod
    def _process_next(self, **kwargs):
        """Callback to execute at __next__ with keyword arguments being the state keys this object listens to."""
        pass

    @abstractmethod
    def _process_stop(self):
        """Callback to execute just before raising StopIteration."""
        pass
