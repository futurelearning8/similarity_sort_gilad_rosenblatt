import cv2
from abc import ABC, abstractmethod
from utils import IteratorWithCallbacks as IteratorWithCallbacksMixin


class Feed(ABC):
    """An interface for an image feed."""

    def __init__(self, name):
        """
        Initialize a Feed mixin and save its name.

        :param str name: name of the feed.
        """
        assert isinstance(name, str)
        self._name = name

    @abstractmethod
    def get_frame(self):
        """Return a new frame from the feed."""
        pass

    @abstractmethod
    def start(self):
        """Start the feed."""
        pass

    @abstractmethod
    def stop(self):
        """Stop the feed."""
        pass

    @property
    def name(self):
        """
        The name string of the feed.

        :return str: name of the feed.
        """
        return self._name


class FeedIteratorWithCallbacks(IteratorWithCallbacksMixin, Feed, ABC):
    """
    Feed that generates frames when iterated over and can register listener callbacks to execute at each iteration.
    To implement the class one must only implement get_frame, start and stop.
    """

    def __init__(self, name, max_frames=None):
        """
        Initialize an IteratorWithCallbacks that iterates up to number_of_frame. For each iteration it updates an
        internal state parameter called frame with the output of the feed get_frame(). Optionally set the max number
        of frames to pull from the feed is the input number of iterations (after which the iterator is exhausted).

        :param str name: name of the feed.
        :param int max_frames: limit on the number of frames to pull from the feed (use None if unlimited).
        """

        # Initialize the iterator w/callbacks mixin first and then the feed parent class.
        super().__init__(
            name=name,
            limit=max_frames
        )

        # Add a state parameter to the iterator state dictionary to store the current frame.
        self.add_state(
            key="frame",
            init_value=None,
            update_method=self._frame_update_method
        )
        
    def __iter__(self):
        """ Start the feed before calling iter on iterator w/callbacks mixin."""
        self.start()
        return super(FeedIteratorWithCallbacks, self).__iter__()

    def __next__(self):
        """ Stop the feed after calling next on iterator w/callbacks mixin if it raises StopIteration."""
        try:
            return super(FeedIteratorWithCallbacks, self).__next__()
        except StopIteration:
            self.stop()
            raise StopIteration

    def _frame_update_method(self, previous_frame):
        """
        A wrapper for get_frame that updates the "frame" state parameter in the iterator state dictionary.

        :param previous_frame: the value of the frame state parameter which is to be updated during __next__.

        NOTE: Argument previous_frame is there to conform to the callback function signature the iterator expects.
        """
        return self.get_frame()

    @property
    def number_of_frames(self):
        """The number of frames this feed generates before feed is exhausted."""
        return self.limit


class VideoFeed(FeedIteratorWithCallbacks):
    """An image feed read from a video capture."""

    def __init__(self, capture_this, name):
        """
        Save source of video feed.

        :param capture_this: input to cv2.VideoCapture to capture frames from (e.g., a filename of a video file).
        """
        super().__init__(name)
        self._source = capture_this
        self._capture = None

    def get_frame(self):
        """Get new frame from video feed."""

        # Try to get a new frame from the capture.
        retrieved, frame = self._capture.read()

        # Terminate and release capture if frame was not read correctly (retrieved == False, frame == None).
        if not retrieved:
            self.terminate()  # Next call to __next__ will raise StopIteration
            return None

        # Return frame.
        return frame

    def start(self):
        """Start the a video capture."""
        self._capture = cv2.VideoCapture(self._source)

    def stop(self):
        """Release the video capture."""
        self._capture.release()


class FileFeed(FeedIteratorWithCallbacks):
    """
    Feed that iterates over images it reads from files located in a folder corresponding to a named image sequence
    (navigated using DataParser) and can register listener callbacks to execute at each iteration.
    """

    def __init__(self, name, number_of_frames, filenames):
        """
        Initialize a FileFeed that read image frame from a list of filenames.

        :param str name: name of the feed.
        :param int number_of_frames: number of frames to pull from the feed.
        :param Generator filenames: filename generator for frames sorted by ascending frame numbers.
        """

        # Initialize an FeedIteratorWithCallbacks.
        super().__init__(
            name=name,
            max_frames=number_of_frames
        )

        # Save an initialized iterator that returns filename of next frame (sorted in ascending frame numbers).
        self._filenames = iter(filenames)

    def get_frame(self):
        """
        Return next frame from the feed corresponding to the next filename.

        :return: frame as a cv2 image.
        """
        return cv2.imread(next(self._filenames))

    def start(self):
        """Start the feed: nothing to do here."""
        pass

    def stop(self):
        """Stop the feed: nothing to do here."""
        pass


class StubFeed(FeedIteratorWithCallbacks):
    """
    Stub feed that iterates over running frame numbers and can register listener callbacks to execute at each iteration.
    """

    def __init__(self, name, number_of_frames):
        """
        Initialize a FileFeed that read image frame from a list of filenames.

        :param str name: name of the feed.
        :param int number_of_frames: number of frames to pull from the feed.
        """
        super().__init__(
            name=name,
            max_frames=number_of_frames
        )

    def get_frame(self):
        """
        Return the 1-based running number of the new frame.

        :return: frame number (starting from 1).
        """
        return self.index + 1

    def start(self):
        """Start the feed: nothing to do here."""
        pass

    def stop(self):
        """Stop the feed: nothing to do here."""
        pass
