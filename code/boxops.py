import numpy as np
from numba import jit
from abc import ABC, abstractmethod


class BoxConvertor:
    """
    Container for conversion methods between bounding box representations.
    For more on bounding box encodings, see <https://leimao.github.io/blog/Bounding-Box-Encoding-Decoding/>.

    Corners representation (default box representation):
        [x1, y1, x2, y2] where x1, y1 is the top left corner and x2, y2 is the bottom right one.

    Centroid representation:
        [cx, cy, s, r] where cx, cy is the box center, s is the scale (area) and r is the aspect ratio.

    NOTE: x-y are defined on an image coordinate system: x growing from left to right and y growing from top to bottom.
    """

    @staticmethod
    def to_centroid(box, column_output=False):
        """
        Convert a bounding box in the corners form to the centroid form. See class docstring for formats.

        :param np.ndarray box: a row/column 4-vector representing a bounding box in corners format [x1, y1, x2, y2].
        :param bool column_output: a boolean indicating if the return value is a column vector or not (row).
        :return np.ndarray: a 4-vector representing a bounding box in centroid format [cx, cy, s, r].
        """
        w = box[2] - box[0]
        h = box[3] - box[1]
        x = box[0] + w / 2.0
        y = box[1] + h / 2.0
        s = w * h
        r = w / float(h)
        if column_output:
            sz = (4, 1)
        else:
            sz = (1, 4)
        return np.array([x, y, s, r]).reshape(sz)

    @staticmethod
    def from_centroid(x, column_output=False):
        """
        Convert a bounding box in the centroid form to the corners form. See class docstring for formats.

        :param np.ndarray x: a row/column 4-vector representing a bounding box in centroid format [cx, cy, s, r].
        :param bool column_output: a boolean indicating if the return value is a column vector or not (row).
        :return np.ndarray: a 4-vector representing a bounding box in corners format [x1, y1, x2, y2].
        """
        w = np.sqrt(x[2] * x[3])
        h = x[2] / w
        x1 = x[0] - w / 2.0
        y1 = x[1] - h / 2.0
        x2 = x[0] + w / 2.0
        y2 = x[1] + h / 2.0
        if column_output:
            sz = (4, 1)
        else:
            sz = (1, 4)
        return np.array([x1, y1, x2, y2]).reshape(sz)


class BoxMetric(ABC):
    """Interface for metric operations on bounding boxes."""

    @staticmethod  # Design choice: keep distance a static method to boost performance (called O(n^3) times in SORT).
    @abstractmethod
    def distance(box1, box2):
        """Calculate the (non-negative real) distance between two bounding boxes using the corners representation."""
        pass


class IOU(BoxMetric):
    """Implements the BoxMetric interface using Intersection Over Union (IOU)."""

    @staticmethod
    @jit  # Just-in-time compilation (reduces SORT runtime by ~23%).
    def distance(box1, box2):
        """
        Compute Intersection Over Union (IOU) between two bounding boxes.

        :param np.ndarray box1: a row/column 4-vector representing 1st bounding box in corners format [x1, y1, x2, y2].
        :param np.ndarray box2: a row/column 4-vector representing 2nd bounding box in corners format [x1, y1, x2, y2].
        :return float: non-negative IOU distance between box1 and box2.
        """
        xx1 = np.maximum(box1[0], box2[0])
        yy1 = np.maximum(box1[1], box2[1])
        xx2 = np.minimum(box1[2], box2[2])
        yy2 = np.minimum(box1[3], box2[3])
        w = np.maximum(0.0, xx2 - xx1)
        h = np.maximum(0.0, yy2 - yy1)
        wh = w * h
        o = wh / ((box1[2] - box1[0]) * (box1[3] - box1[1])
                  + (box2[2] - box2[0]) * (box2[3] - box2[1]) - wh)
        return o


class Euclidean(BoxMetric):
    """Implements the BoxMetric interface using Euclidean distance."""

    @staticmethod
    def distance(box1, box2):
        """
        Compute Euclidean distance between centroids of two bounding boxes.

        :param np.ndarray box1: a row/column 4-vector representing 1st bounding box in corners format [x1, y1, x2, y2].
        :param np.ndarray box2: a row/column 4-vector representing 2nd bounding box in corners format [x1, y1, x2, y2].
        :return float: non-negative DBC distance between box1 and box2.
        """

        # Calculate centroids (using BoxConvertor.to_centroid() with @jit leads to a NumbaDeprecationWarning).
        xc1, yc1, _, _ = BoxConvertor.to_centroid(box1)[0]
        xc2, yc2, _, _ = BoxConvertor.to_centroid(box2)[0]

        # Return Euclidean distance between centroids.
        return np.sqrt((xc1 - xc2)**2 + (yc1 - yc2)**2)
