from abc import ABC, abstractmethod


class Detector(ABC):
    """Detector for objects in an image."""

    @abstractmethod
    def detect(self, image):
        """Detect objects in a given image and return their corresponding bounding boxes."""
        pass


class StubDetector(Detector):
    """A stub detector that caches an iterator of pre-detected bounding boxes to return at each call to detect."""

    def __init__(self, boxes_generator):
        """Cache detection boxes generator for pre-detected bounding boxes.

        :param collections.abc.Iterator boxes_generator: generates np.ndarray with detection bounding boxes per frame.
        """
        self._cached_boxes = iter(boxes_generator)

    def detect(self, image):
        """
        Return detection boxes for "this image" using the cached bounding boxes generator. Input image is ignored.

        :param image: the image for which to generate detection boxes (a stub in this class - completely ignored).
        :return np.ndarray: array of bounding boxes for objects detected in input image (see note in method docstring).

        NOTE: Syncing the generator output with input image is NOT done here and must be taken care of by the caller.
        """
        return next(self._cached_boxes)
